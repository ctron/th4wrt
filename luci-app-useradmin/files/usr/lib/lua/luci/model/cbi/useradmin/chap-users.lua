
m = Map("users", "Users")

s = m:section(TypedSection, "chap-user", "CHAP User")
s.addremove = true
s.anonymous = true
s.template = "cbi/tblsection"

name = s:option(Value, "name", "name", "Name of the client. Leave empty to match any.")
name.rmempty = true

s:option(Value, "provider", "provider", "Name of the provider. Leave empty to match any.")
s:option(Value, "password", "password", "Password. Leave empty for no password.")

ipaddress = s:option(Value, "ipaddress", "ipaddress", "The allowed IP addresses. Leave empty for all (*).")
ipaddress.rmempty = true

return m

